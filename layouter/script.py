from os.path import dirname, join

from layouter.components.adjuster import Adjuster
from layouter.components.layouter import Layouter
from layouter.components.loader import Loader
from layouter.components.previewer import Previewer


def main():
    images = Loader(join(dirname(__file__), '..', 'files')).load()
    print(f'Loaded {len(images)} images')
    images = Adjuster(images).adjust()
    layout_images = Layouter(images).layout()
    Previewer(layout_images).preview()


if __name__ == '__main__':
    main()
