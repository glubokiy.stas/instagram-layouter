from os import listdir
from os.path import join, isdir

from PIL import Image


class Loader:

    def __init__(self, image_dir):
        self.image_dir = image_dir

    def load(self):
        paths_in_image_dir = [join(self.image_dir, filename) for filename in listdir(self.image_dir)]
        image_paths = [p for p in paths_in_image_dir if not isdir(p)]
        return [Image.open(image_path) for image_path in image_paths]
