from random import shuffle

from layouter.components.comparer import Comparer


class Layouter:

    def __init__(self, images):
        self.images = images
        self.n = len(images)
        self.num_pairs = self.n // 3
        self.comparer = Comparer(images)
        self.paired_idxs = set()

    def layout(self):
        best_pairs = self.get_best_pairs()
        singles_list = [idx for idx in range(self.n) if idx not in self.paired_idxs]
        shuffle(singles_list)

        image_order = []
        for pair, single in zip(best_pairs, singles_list):
            image_order.extend([pair[0], single, pair[1]])

        images = [self.images[idx] for idx in image_order]

        return images

    def get_best_pairs(self):
        pairs = []
        for first_idx, second_idx, similarity in self.comparer.similarity_list:
            if first_idx not in self.paired_idxs and second_idx not in self.paired_idxs:
                pairs.append((first_idx, second_idx))
                self.paired_idxs.add(first_idx)
                self.paired_idxs.add(second_idx)

                if len(pairs) == self.num_pairs:
                    return pairs

