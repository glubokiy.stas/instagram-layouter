class Adjuster:

    def __init__(self, images):
        self.images = images

    def adjust(self):
        images = self.images
        return [image.resize((200, 200)) for image in images]
