from math import ceil

from PIL import Image


class Previewer:

    def __init__(self, images):
        self.images = images

    def preview(self, image_width=100, margin=5):
        image_height = image_width
        thumbnails = [image.resize((image_width, image_height)) for image in self.images]

        num_width = 3
        grid_width = image_width * num_width + margin * (num_width - 1)

        num_height = ceil(len(thumbnails) / 3)
        grid_height = image_height * num_height + margin * (num_height - 1)

        grid_image = Image.new('RGB', (grid_width, grid_height), color=(255, 255, 255))

        for i, thumbnail in enumerate(thumbnails):
            row = i // 3
            column = i % 3

            horizontal_offset = column * (image_width + margin)
            vertical_offset = row * (image_height + margin)

            grid_image.paste(thumbnail, (horizontal_offset, vertical_offset))

        grid_image.show()
