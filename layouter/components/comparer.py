class Comparer:

    def __init__(self, images):
        self.images = images
        self.n = len(images)
        self.hists = [self.get_hists(image) for image in images]

    @property
    def similarity_list(self):
        result = []

        for i in range(self.n):
            for j in range(i + 1, self.n):
                result.append((i, j, self.similarity_between(i, j)))

        result.sort(key=lambda x: x[2], reverse=True)
        return result

    def similarity_between(self, first_idx, second_idx):
        first_hist = self.hists[first_idx]
        second_hist = self.hists[second_idx]
        num_buckets = len(first_hist[0])

        total = 0
        for channel in range(3):
            first = first_hist[channel]
            second = second_hist[channel]
            for i in range(num_buckets):
                total += (first[i] - second[i]) ** 2

        return 1 - total / (3 * num_buckets)

    def get_hists(self, image, num_buckets=3):
        hists = []

        pixels = self.get_valid_pixels(image, margin=5)
        for channel in range(3):
            channel_hist = [0 for _ in range(num_buckets)]
            for pixel in pixels:
                bucket = pixel[channel] * num_buckets // 256
                channel_hist[bucket] += 1

            channel_hist = [x / len(pixels) for x in channel_hist]
            hists.append(channel_hist)

        return hists

    def get_valid_pixels(self, image, margin=16):
        valid_pixels = []
        for r, g, b in image.getdata():
            too_bright = all(channel > 255 - margin for channel in [r, g, b])
            too_dark = all(channel < margin for channel in [r, g, b])
            if not too_bright and not too_dark:
                valid_pixels.append((r, g, b))
        return valid_pixels

